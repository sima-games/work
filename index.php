<?php
    echo '<h2>CodeAbbey Tasks:</h2>';

    $path = '/xampp/htdocs/';
    $files = scandir($path);

    $exceptions = array(".", "..", ".git", "index.php", ".gitignore");

    $files = check_on_exceptions($files, $exceptions);

    $count = 0;
    foreach($files as $file){
        $count++;
        echo '<a href="'.$file.'">'. $count.') '. get_filename($file).'</a><br>';
    }

    function get_filename($file){
        return ucfirst(preg_replace('/[-]/', ' ', preg_split('/[.]/', $file)[0]));
    }

    function check_on_exceptions($files, $exceptions){
        for($i = 0; $i < sizeof($files); $i++){
            foreach($exceptions as $e){
                if($files[$i] == $e){
                    \array_splice($files, $i, 1);
                }
            }
        }
        return $files;
    }
?>