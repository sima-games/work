<?php
    $input = "5 14 29 16 19 69 6 9 89 12 16 80 8 20 92 25 11 12 7 6 56 3 9 75 12 3 33";
    $arr = explode(" ", $input);
    $res = range(1, sizeof($arr)/3);

    echo "input data:".
        "<br>".
        sizeof($res).
        "<br>";

    for($i = 0; $i < sizeof($res); $i++){
        $a = $arr[$i*3];
        $b = $arr[$i*3+1];
        $c = $arr[$i*3+2];

        $res[$i] = progression($a, $b, $c);
        echo $a. " ". $b. " ". $c. "<br>";
    }

    echo "<br>". 
        "answer:".
        "<br>".
        implode(" ", $res).
        "<br>";

    function progression($a, $b, $c){
        return $c > 0 ? $a + progression($a + $b, $b, $c-1) : 0;
    }
?>