<?php
    $input = "1913522 -554271 1303730 -7070378 8091506 -5111695 -3482690 -2510898 8972590 2510761 -6256358 6797349 7892049 7110675 -7692540 -4234429 -4998463 598721 -3071250 6726616 -1863389 -4501861 2132349 -711979 -5095661 4989843 8249213 -4880621 3493210 1622619 -9424386 -4593267 -8931652 1879344 -1663646 9159854 6767649 4853664 -3351044 5740239 -2635575 392598 2537589 -4743525 -2496727 4845049 1022045 2504810 -4556230 7950794 -768573 3580380 -6551067 -8636224 -7131598 -1646728";
    $arr = explode(" ", $input);
    $res = range(1, sizeof($arr)/2);

    echo "data:".
        "<br>".
        sizeof($res).
        "<br>";

    for($i = 0; $i < sizeof($res); $i++){
        $a = $arr[$i*2];
        $b = $arr[$i*2+1];

        $res[$i] = min_of_two($a, $b);
        echo $a. " ". $b. "<br>";
    }

    echo "<br>". 
        "answer:".
        "<br>".
        implode(" ", $res).
        "<br>";

    function min_of_two($a, $b){
        return $a < $b ? $a : $b;
    }
?>