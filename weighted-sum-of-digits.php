<?php
    $input = "30 13 2531 48 111335 130 15146 60307600 35681 1690 49378103 56 911 190 75 1496128 27724320 7225780 2815718 66970 13 8599616 5116887 2512 6315113 21 10 12 983 4208161 4 59313629 640 239 1761235 799348 412140";
    $arr = explode(" ", $input);
    $res = array();

    echo "input data:".
        "<br>".
        sizeof($arr).
        "<br>";

    foreach($arr as $i){
        array_push($res, weighted_sum_of_digits($i));
        echo $i . " ";
    }

    echo "<br>". 
        "<br>". 
        "answer:".
        "<br>".
        implode(" ", $res).
        "<br>";

    function weighted_sum_of_digits($i) : int {
        $nums = str_split($i);
        $w_sum = 0;

        for($i = 0; $i < sizeof($nums); $i++){
            $w_sum+=$nums[$i]*($i+1);
        }
        
        return $w_sum;   
    }
?>