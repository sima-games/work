<?php
    $input = "105 128 281 408 346 531 968 1282 2505 908 1424 1752 717 897 1628 610 1149 721 735 791 536 1451 2265 934 1300 702 642 244 392 215 137 426 270 442 509 537 882 1112 2593 173 365 320 426 403 471 945 1558 915 611 826 1178 301 341 226 644 1050 674 1449 3222 920";
    $arr = explode(" ", $input);
    $res = range(1, sizeof($arr)/3);

    echo "input data:".
        "<br>".
        sizeof($res).
        "<br>";

    for($i = 0; $i < sizeof($res); $i++){
        $a = $arr[$i*3];
        $b = $arr[$i*3+1];
        $c = $arr[$i*3+2];

        $res[$i] = is_triangle_possible($a, $b, $c); 

        echo $a. " ". $b. " ". $c. "<br>";
    }

    echo "<br>". 
        "answer:".
        "<br>".
        implode(" ", $res).
        "<br>";

    function is_triangle_possible($a, $b, $c) : int {
        return $a < $b + $c and $b < $a + $c and $c < $a + $b ? 1 : 0;
    }
?>