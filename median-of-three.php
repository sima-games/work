<?php
    $input = "840 835 820 163 67 7 2 413 479 518 527 528 140 136 145 725 727 3 541 93 16 435 467 433 708 646 15 24 17 15 829 836 823 17 14 8 77 76 99 6 1 444 3 9 299 1 20 26 76 10 80 9 179 100 6 16 87 9 18 5 1649 671 1640 212 263 170 50 618 527 521 55 421 68 124 61 1285 605 1376 68 1058 1041 708 609 611 1047 1046 56 244 2 32";
    $arr = explode(" ", $input);
    $res = range(1, sizeof($arr)/3);

    echo "input data:".
        "<br>".
        sizeof($res).
        "<br>";

    for($i = 0; $i < sizeof($res); $i++){
        $a = $arr[$i*3];
        $b = $arr[$i*3+1];
        $c = $arr[$i*3+2];

        $res[$i] = median_of_three($a, $b, $c);
        echo $a. " ". $b. " ". $c. "<br>";
    }

    echo "<br>". 
        "answer:".
        "<br>".
        implode(" ", $res).
        "<br>";

    function median_of_three($a, $b, $c){
        if($a > $b and $a < $c or $a < $b and $a > $c){
            return $a;
        }else if($b > $a and $b < $c or $b < $a and $b > $c){
            return $b;
        }else{
            return $c;
        }
    }
?>