<?php
    $input = "110 1.83 81 1.62 74 1.66 100 2.89 103 1.72 40 1.53 119 2.57 89 2.22 94 2.69 58 2.30 96 1.89 92 1.67 101 2.25 91 2.75 44 1.45 111 1.78 59 1.68 96 1.82 120 2.34 100 1.77 67 1.47 42 1.49 41 1.21 87 1.73 99 1.73 96 2.09 74 1.92 61 1.95 97 1.66 60 1.39 63 2.12 95 1.95 83 1.99 56 1.50";
    $arr = explode(" ", $input);
    $res = range(1, sizeof($arr)/2);

    echo "input data:".
        "<br>".
        sizeof($res).
        "<br>";

    for($i = 0; $i < sizeof($res); $i++){
        $weight = $arr[$i*2];
        $height = $arr[$i*2+1];

        $index = BMI($weight, $height);
        $conclusion =  BMI_conclusion($index);

        $res[$i] = $conclusion;
        echo $weight. " ". $height. "<br>";
    }

    echo "<br>". 
        "answer:".
        "<br>".
        implode(" ", $res).
        "<br>";

    function BMI_conclusion($index) : string {
        if($index < 18.5){
            return "under";
        }else if($index < 25){
            return "normal";
        }else if($index < 30){
            return "over";
        }else{
            return "obese";
        }
    }

    function BMI($weight , $height) : float {
        return $weight / ($height * $height);
    }
?>