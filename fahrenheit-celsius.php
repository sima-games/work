<?php
    $input = "36 409 124 420 199 76 300 317 499 420 179 344 418 369 519 46 454 567 362 464 505 424 422 550 316 459 266 483 479 175 352 230 553 445 49 151 490";
    $arr = explode(" ", $input);

    echo "input data:".
        "<br>".
        $input.
        "<br>".
        "answer:".
        "<br>";

    \array_splice($arr, 0, 1);

    foreach($arr as $i){
        echo round(fahrenheit_to_celsius($i)). " ";
    } 

    function fahrenheit_to_celsius($f){
        return ($f - 32) * 5/9;   
    }
?>