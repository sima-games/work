<?php
    $input = "265 186 16 185 216 163 173 122 194 395 97 197 183 68 22 386 239 37 87 256 191 173 253 61 341 282 155 101 122 103";
    $arr = explode(" ", $input);
    $res = range(1, sizeof($arr)/3);

    echo "input data:".
        "<br>".
        sizeof($res).
        "<br>";

    for($i = 0; $i < sizeof($res); $i++){
        $a = $arr[$i*3];
        $b = $arr[$i*3+1];
        $c = $arr[$i*3+2];

        $form = $a * $b + $c;

        $res[$i] = sum_of_digits($form);
        echo $a. " ". $b. " ". $c. "<br>";
    }

    echo "<br>". 
        "answer:".
        "<br>".
        implode(" ", $res).
        "<br>";

    function sum_of_digits($i){
        $sum = 0;
        while($i != 0){
            $sum += $i % 10;
            $i /= 10;
        }
        return $sum;   
    }
?>