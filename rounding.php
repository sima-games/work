<?php
    $input = "17303 1840 5993 890 4415060 30 1787881 765 5472424 831 7419 1892 9277 1188 -776652 917278 7645817 286 1544853 122 7797274 4526868 4805389 -4227880 18443 1130";
    $arr = explode(" ", $input);
    $res = range(1, sizeof($arr)/2);

    echo "input data:".
        "<br>".
        sizeof($res).
        "<br>";

    for($i = 0; $i < sizeof($res); $i++){
        $a = $arr[$i*2];
        $b = $arr[$i*2+1];

        $res[$i] = round(divide($a, $b));
        echo $a. " ". $b. "<br>";
    }

    echo "<br>". 
        "answer:".
        "<br>".
        implode(" ", $res).
        "<br>";

    function divide($a, $b){
        return $a / $b;
    }
?>